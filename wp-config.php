<?php
/**
 * In dieser Datei werden die Grundeinstellungen für WordPress vorgenommen.
 *
 * Zu diesen Einstellungen gehören: MySQL-Zugangsdaten, Tabellenpräfix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es
 * auf der {@link http://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Informationen für die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgeführt,
 * wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von
 * wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden möchtest. */
define('DB_NAME', 'd3s');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'db');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', 'bichewgy');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', 'localhost');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht geändert werden */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden KEY in eine beliebige, möglichst einzigartige Phrase.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle KEYS generieren lassen.
 * Bitte trage für jeden KEY eine eigene Phrase ein. Du kannst die Schlüssel jederzeit wieder ändern,
 * alle angemeldeten Benutzer müssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         'xP=(u|Z# wpk!}DsTQa?S^G:9~b>Cwr5<=%$nrlR1IMIy4RmL=g6 B9q-6VY:&h8');
define('SECURE_AUTH_KEY',  'zGV`*|plF-+7~?0d}ShPqA9 /8MdNc^0GS9sJj)Pz_L}+c`r-gW+R+|LeuXp+>J<');
define('LOGGED_IN_KEY',    '#0H.~7`F5|=SNaR^>65u*_}E+M9<Y=U/ow;XG)C]yDUl7Oyh]VYL5FOiY{Krql+>');
define('NONCE_KEY',        '&6{lw]9;[x+<=&QgH,k=-!4- 2#`(+6TQ$Ige/0R1![4NR:C_]OdHl.(d8mE+iHN');
define('AUTH_SALT',        'T/+[ou j1K]l9N[Hg>z#SV--aHy3!P[G:`FF/-zviq)):fFD;r=*55|JUG/^en?!');
define('SECURE_AUTH_SALT', 'w82_~96tT)=?a&&e](Mk/d-=J[68K%_+K?$`o3E3<}j0eY|>X0kA#Y(KV<4jsJ0n');
define('LOGGED_IN_SALT',   '(gN5NI_i(,EuSXT7Qc~s_-.m|M-+_KUusAbUo_bq d|y+1UPtL<7o Zy7VnIU r>');
define('NONCE_SALT',       'v><An~t*C`erTCk6Er>P)J^NKL9<N|hI<pjq+7v;PLnymJ|Dm^q cGCq-./27V4s');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 *  Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_ENV', 'development');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
