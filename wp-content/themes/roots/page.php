<div class="red visible-xs"></div><!-- .red -->

<?php 
/*
|--------------------------------------------------------------------------
| Startseite
|--------------------------------------------------------------------------
*/
if(is_front_page()): get_template_part("templates/start"); endif;
?>

<?php
/*
|--------------------------------------------------------------------------
| Content Seite
|--------------------------------------------------------------------------
*/
if(!is_front_page()):
while (have_posts()) : the_post(); 	get_template_part('templates/content', 'page'); endwhile;
endif;
?>
