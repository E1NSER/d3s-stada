<section id="intro">
	<div class="container">
		
		<!------------------------------------------------------------------------------- -->
		<!-- Welcome -->
		<!------------------------------------------------------------------------------- -->
		<div class="row">
			<div class="col-sm-12 welcome">
				<?php while (have_posts()) : the_post(); ?>
				  <div class="headline"><h1><?php the_title(); ?></h1></div><!-- .headline -->
				<div class="subheadline"><h2><?php the_content(); ?></h2></div><!-- .subheadline -->
				<?php endwhile; ?>
			</div><!-- .welcome -->
		</div><!-- .row -->
		
		<!------------------------------------------------------------------------------- -->
		<!-- Personen -->
		<!------------------------------------------------------------------------------- -->
		<div class="row">
			<div class="personen">
				<div class="col-sm-4 person frau">
					<div class="content">
						<img src="<?php bloginfo("template_url"); ?>/assets/img/frau.png" alt="<?php bloginfo("name"); ?>"/>
					</div><!-- .content -->
				</div><!-- .person -->
				<div class="col-sm-4 person kind hidden-xs">
					<div class="content">
						<img src="<?php bloginfo("template_url"); ?>/assets/img/kind.png" alt="<?php bloginfo("name"); ?>"/>
					</div><!-- .content -->
				</div><!-- .person -->
				<div class="col-sm-4 person mann hidden-xs">
					<div class="content">
						<img src="<?php bloginfo("template_url"); ?>/assets/img/mann.png" alt="<?php bloginfo("name"); ?>"/>
					</div><!-- .content -->
				</div><!-- .person -->

			</div><!-- .peronen -->
		</div><!-- .row -->
		
		
		
	</div><!-- .container -->
</section><!-- #intro -->