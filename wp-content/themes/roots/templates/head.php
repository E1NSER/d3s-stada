<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php bloginfo("template_url"); ?>/assets/img/favicon.ico" rel="icon" type="image/x-icon" />

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">

  <!------------------------------------------------------------------------------- -->
  <!-- Google Fonts -->
  <!------------------------------------------------------------------------------- -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
 
  

  <?php wp_head(); ?>
</head>
