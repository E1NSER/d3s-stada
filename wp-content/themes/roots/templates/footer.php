<footer class="content-info" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 welle">
				<img src="<?php bloginfo("template_url"); ?>/assets/img/welle.png"/>
				
				<!------------------------------------------------------------------------------- -->
				<!-- Packshot -->
				<!------------------------------------------------------------------------------- -->
				<div class="packshot">
				<?php while (have_posts()) : the_post(); ?>
					<?php the_post_thumbnail(); ?>
				<?php endwhile; ?>
				
				</div><!-- .packshot -->
				
			</div><!-- .welle -->
		</div><!-- .row -->
		
		<!------------------------------------------------------------------------------- -->
		<!-- Meta -->
		<!------------------------------------------------------------------------------- -->
		<div class="row">
			<div class="col-sm-6 copyright">
				© <?php the_time("Y"); ?> <a href="<?php bloginfo("url"); ?>" title="<?php bloginfo("name"); ?>"><?php bloginfo("name"); ?></a>
				<nav class="footer-nav">
					<?php wp_nav_menu(array("menu" => " Footer ","menu_class" => "list-inline")); ?>
				</nav>
			</div><!-- .copyright -->
			<div class="col-sm-6 logo"><a href="http://www.stada.at" target="_blank"><img src="<?php bloginfo("template_url"); ?>/assets/img/logo.png" alt="<?php bloginfo("name"); ?>" /></a></div><!-- .logo -->
		</div><!-- .row -->
		
		
	</div><!-- .container -->
	
</footer>

