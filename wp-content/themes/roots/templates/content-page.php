<!------------------------------------------------------------------------------- -->
<!-- Content -->
<!------------------------------------------------------------------------------- -->
<section id="main-content" class="content-page">
	<div class="container">
		
		
			<?php if( get_field("col_right") ): ?>
				
				<div class="content-holder">
					<div class="row">
						<div class="col-xs-offset-1 col-xs-10">
							<div class="headline"><h1><?php the_title(); ?></h1></div><!-- .headline -->
						</div>
					</div>
					<div class="row">
						<div class="col-md-offset-1 col-md-5">
							<div class="text"><?php the_content(); ?></div><!-- .text -->
						</div>
						<div class="col-md-5">
							<div class="text"><?php the_field("col_right"); ?></div><!-- .text -->
						</div>
					</div>
					
					
				</div><!-- .content-holder -->
			
			<?php else: ?>
			<div class="row">
				<div class="col-xs-offset-1 col-xs-10">
					<div class="content-holder">
						<div class="headline"><h1><?php the_title(); ?></h1></div><!-- .headline -->
						<div class="text"><?php the_content(); ?></div><!-- .text -->
					</div><!-- .content-holder -->
				</div><!-- .col-sm-12 -->
			</div><!-- .row -->
			<?php endif; ?>
		
			
		
	</div><!-- .container -->
</section><!-- .main-content -->